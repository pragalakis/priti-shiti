# [deprecated] Priti Shiti

Moved to codeberg: https://codeberg.org/evasync/priti-shiti

https://pragalakis.gitlab.io/priti-shiti/

![screenshot](priti-shiti.png)

## What's inside?

- react - a frontend framework
- parcel - a build tool
- canvas api - to draw on the screen 2d graphics
- audio api - to play the sound

## How does it work?

On the mouse position we render Priti Patel's image.
There are also 200 flies that are rendered on a random spot in the screen and they rotate & move towards the mouse position.

All the images are drawn every time we move the mouse and in every animation frame (using the `requestAnimationFrame`).

All the flies start with a random position and random acceleration.
As we render them the acceleration continues to change randomly and there is also a chance for some of them to disappear and reappear on the screen (just to create an infinite fly illusion - although not as smooth as it can be).

## How to run it?

- Clone this repo
- Install all the dependencies: `npm install`
- Run the site locally: `npm start`
- Build the site: `npm run build`
