import React, { useEffect, useState, useLayoutEffect, useRef } from 'react';
import imgRef from '/src/assets/priti_patel.png';
import flyImgRef from '/src/assets/fly.png';
import Audio from './Audio.jsx';
import './styles.css';

const width = window.innerWidth;
const height = window.innerHeight;
const pixelRatio = window.devicePixelRatio;
const displayWidth = Math.floor(pixelRatio * width);
const displayHeight = Math.floor(pixelRatio * height);
const style = { width, height };

const range = (min, max) => {
  return Math.random() * (max - min) + min;
};

const fliesData = Array(200)
  .fill()
  .map(() => {
    const xPosition = range(-displayWidth, 2 * displayWidth);
    const yPosition = range(-displayHeight, 2 * displayHeight);
    const accelerationX = 0.01 * Math.random();
    const accelerationY = 0.01 * Math.random();
    return {
      x: xPosition,
      y: yPosition,
      ax: accelerationX,
      ay: accelerationY
    };
  });

const App = () => {
  const [ctx, setCtx] = useState(false);
  const [frame, setFrame] = useState(0);
  const [image, setImage] = useState(false);
  const [flyImage, setFlyImage] = useState(false);
  const [mousePosition, setMousePosition] = useState({ x: 0, y: 0 });

  const canvas = useRef(null);
  const reqAnimationFrameRef = useRef(null);

  const loadMainImage = () => {
    const img = new Image();
    img.src = imgRef;
    return (img.onload = () => {
      setImage(img);
    });
  };

  const loadFlyImage = () => {
    const img = new Image();
    img.src = flyImgRef;
    return (img.onload = () => {
      setFlyImage(img);
    });
  };

  const animation = () => {
    setFrame(reqAnimationFrameRef.current);
    reqAnimationFrameRef.current = requestAnimationFrame(animation);
  };

  // draw
  useEffect(
    () => {
      const mainImgWidth = 130;
      const mainImgHeight = 130;
      const flyImgWidth = 65;
      const flyImgHeight = 65;

      if (Boolean(flyImage) && Boolean(image) && Boolean(mousePosition.x)) {
        // clear the background
        ctx.fillRect(0, 0, displayWidth, displayHeight);

        // draw the main cursor image
        ctx.drawImage(
          image,
          mousePosition.x - mainImgWidth / 2,
          mousePosition.y - mainImgHeight / 2,
          mainImgWidth,
          mainImgHeight
        );

        // draw the animated flies
        fliesData.forEach(fly => {
          // new position towards the mouse position
          fly.ax =
            (mousePosition.x - flyImgWidth / 2 - fly.x) *
            (0.03 * Math.random());
          fly.ay =
            (mousePosition.y - flyImgHeight / 2 - fly.y) *
            (0.03 * Math.random());
          fly.x += fly.ax;
          fly.y += fly.ay;

          ctx.save();
          ctx.translate(fly.x + flyImgWidth / 2, fly.y + flyImgHeight / 2);
          const angle = Math.atan2(
            mousePosition.y - fly.y,
            mousePosition.x - fly.x
          );
          ctx.rotate(angle + 90 * (Math.PI / 180));
          ctx.translate(-fly.x - flyImgWidth / 2, -fly.y - flyImgHeight / 2);

          ctx.drawImage(flyImage, fly.x, fly.y, flyImgWidth, flyImgHeight);
          ctx.restore();

          // randomly remove and respawn a fly (just to keep them coming)
          if (Math.random() > 0.99) {
            fly.x = range(-displayWidth, 2 * displayWidth);
            fly.y = range(-displayHeight, 2 * displayHeight);
          }
        });
      }
    },
    [ctx, frame, image, flyImage, JSON.stringify(mousePosition)]
  );

  // load images and register events
  useEffect(() => {
    loadMainImage();
    loadFlyImage();

    const handleMouseMove = e => {
      setMousePosition({ x: e.pageX * pixelRatio, y: e.pageY * pixelRatio });
    };

    window.addEventListener('mousemove', handleMouseMove, false);
    reqAnimationFrameRef.current = requestAnimationFrame(animation);
    return () => {
      window.removeEventListener('mousemove', handleMouseMove, false);
      cancelAnimationFrame(reqAnimationFrameRef.current);
    };
  }, []);

  // init context and add it to the state
  useLayoutEffect(() => {
    const context = canvas.current.getContext('2d');
    setCtx(context);
    context.fillStyle = 'pink';
  }, []);

  return (
    <main className="container">
      <Audio />
      <canvas
        ref={canvas}
        width={displayWidth}
        height={displayHeight}
        style={style}
      >
        <h1>Priti Shiti</h1>
        <p>
          Enable canvas api to view a swarm of flies chasing Priti Patel's image
          that goes wherever the cursor goes.
        </p>
      </canvas>
    </main>
  );
};

export { App };
